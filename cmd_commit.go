package main

import (
	"compress/zlib"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

func commitCmd(args []string) error {
	wd, err := os.Getwd()
	if err != nil {
		return err
	}

	infos, err := workspace{wd}.listDir()
	if err != nil {
		return err
	}

	db := newDatabase(wd)

	for _, info := range infos {
		if info.IsDir() {
			continue
		}

		f, err := os.Open(info.Name())
		if err != nil {
			return err
		}

		b := blob{f, info.Size()}

		hash, err := db.storeBlob(b)
		if err != nil {
			return err
		}

		log.Print("hash:", hash)

		if err := f.Close(); err != nil {
			return err
		}
	}

	return nil
}

type database struct {
	objectDir string
}

func (db database) storeBlob(b blob) ([]byte, error) {
	tmpF, err := ioutil.TempFile(db.objectDir, "tmp_obj_")
	if err != nil {
		return nil, err
	}

	sum, err := b.store(tmpF)
	if err != nil {
		return nil, err
	}

	sumHex := hex.EncodeToString(sum)

	dstPath := filepath.Join(db.objectDir, sumHex[0:2], sumHex[2:])
	if err := os.MkdirAll(filepath.Dir(dstPath), 0755); err != nil {
		return nil, err
	}

	if err := tmpF.Close(); err != nil {
		return nil, err
	}

	err = os.Rename(tmpF.Name(), dstPath)
	if err != nil {
		return nil, err
	}

	return sum, nil
}

type blob struct {
	src  io.Reader
	size int64
}

func (b blob) store(dst io.Writer) ([]byte, error) {
	dstZ := zlib.NewWriter(dst)

	prefix := objectPrefix(blobObject, b.size)
	if _, err := dstZ.Write([]byte(prefix)); err != nil {
		return nil, err
	}

	pr, pw := io.Pipe()

	errQ := make(chan error)

	h := sha1.New()
	go func() {
		_, err := io.Copy(h, pr)
		errQ <- err
	}()

	srcTee := io.TeeReader(b.src, pw)
	if _, err := io.Copy(dstZ, srcTee); err != nil {
		return nil, err
	}

	if err := pw.Close(); err != nil {
		return nil, err
	}

	if err := <-errQ; err != nil {
		return nil, err
	}

	if err := dstZ.Close(); err != nil {
		return nil, err
	}

	return h.Sum(nil), nil
}

type objectType int

const (
	blobObject objectType = iota + 1
)

func (ot objectType) String() string {
	switch ot {
	case blobObject:
		return "blob"
	default:
		return "unknown"
	}
}

func objectPrefix(ot objectType, size int64) string {
	return fmt.Sprintf("%s %d\000", ot, size)
}

func newDatabase(wd string) database {
	return database{filepath.Join(wd, objectsDir)}
}

type workspace struct {
	wd string
}

func (ws workspace) listDir() ([]os.FileInfo, error) {
	f, err := os.Open(ws.wd)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return f.Readdir(-1)
}
