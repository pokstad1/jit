# Building Git in Go

This is my homework for following along with the book [Building Git](https://shop.jcoglan.com/building-git/). While the book's code is written in Ruby, I've decided to translate the code to Go.

## Install

Make sure you have Go v1.12+ installed and run the following command:

```
go get -u gitlab.com/pokstad1/jit
```

This will install the latest version of jit to your `$GOPATH/bin` folder (make sure to add this to your `PATH` environment variable).

## Usage

To get usage, run the following command: `jit help`
