package main

import (
	"fmt"
	"log"
)

var usageStr = `jit [init|commit|help]
`

func usage(msg string) error {
	if msg != "" {
		log.Print(usageStr)
		log.Fatalf(msg)
	}
	fmt.Println(usageStr)
	return nil
}
