package main

import (
	"log"
	"os"
)

var subcmds = map[string]func(args []string) error{
	"init":   initCmd,
	"commit": commitCmd,
	"help":   func([]string) error { return usage("") },
}

func main() {
	if len(os.Args) < 2 {
		usage("no subcommand provided")
	}

	cmd, ok := subcmds[os.Args[1]]
	if !ok {
		usage("invalid subcommand: " + os.Args[1])
	}

	if err := cmd(os.Args[1:]); err != nil {
		log.Fatal(err)
	}
}
