package main

const (
	gitDir     = ".git"
	objectsDir = gitDir + "/objects"
	refsDir    = gitDir + "/refs"
	tmpDir     = gitDir + "/tmp"
)
