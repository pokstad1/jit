package main

import (
	"os"
	"path/filepath"
)

// init git dir if it doesn't exist
func initCmd(_ []string) error {
	wd, err := os.Getwd()
	if err != nil {
		return err
	}

	for _, p := range []string{
		objectsDir,
		refsDir,
	} {
		if err := os.MkdirAll(filepath.Join(wd, p), 0755); err != nil {
			return err
		}
	}

	return nil
}
